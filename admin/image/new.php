<?php
  if($user == $admin_user && $pass == $admin_pass){
    $message = "ログインに成功しました。";
  }else{
    $message = "ユーザ名とパスワードが必要です。";
  }
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>管理画面トップ</title>
  </head>
  <body>
    <h1>画像アップロード</h1>
    <form action="image-index.php" method="POST" enctype="multipart/form-data">
      <label for="upload">画像のアップロード</label>
      <input type="file" name="upfile" id="upload"><br><br>
      <input type="submit" value="投稿する">
   </form>
  </body>
</html>