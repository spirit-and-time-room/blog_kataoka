<?php
  $mysqli = new mysqli("localhost", "root", "root", "blog");
  if($mysqli->connect_error){
    echo $mysqli->connect_error;
    exit();
  }else{
    $mysqli->set_charset("utf8");
  }

  $sql = "SELECT file_name FROM images";
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>管理画面トップ</title>
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
  </head>
  <body>
    <div class="content">
      <h1>The World</h1>
      <?php if($result = $mysqli->query($sql)){ ?>
        <?php while($row = $result->fetch_assoc()){ ?>
          <p class="image"><img src="<?php echo "admin/upload-img/" . $row["file_name"]; ?>"></p>
        <?php } ?>
      <?php $result->close(); ?>
      <?php } ?>
    </div>
  </body>
</html>